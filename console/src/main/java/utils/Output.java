package utils;

import dev3.bank.entity.News;

public class Output {

    public static void printNews(News news) {
        System.out.println("" +
                "Date: " + news.getDate() + "\t" +
                "Title: " + news.getTitle() + "\t" +
                "Text: " + news.getText());
    }
}
