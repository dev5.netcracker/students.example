package utils;


import dev3.bank.entity.Client;
import dev3.bank.entity.Role;

import java.util.Scanner;

public class Input {

    private static void print(String message) {
        System.out.print("\n" + message);
    }

    public static Client inputClient() {
        Scanner scanner = new Scanner(System.in);
        Client client = new Client();
        print("Input your name: ");
        String name = scanner.nextLine();
        client.setName(name);
        print("Input your surname: ");
        String surname = scanner.nextLine();
        client.setSurname(surname);
        print("Input your phone number: ");
        int phoneNumber = scanner.nextInt();
        try {
            client.setPhoneNumber(phoneNumber);
        } catch (NumberFormatException e) {
            System.out.println("Wrong input phone number");
        }
        client.setRole(Role.CLIENT);
        return client;
    }
}
