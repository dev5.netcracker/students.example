package dev3.bank.dao.interfaces;

import dev3.bank.entity.Client;

public interface ClientDAO extends CrudDAO<Client> {

}

